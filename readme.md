These are the homework assignments for the course "Natural Language Processing" at the University of applied sciences in Utrecht.  
The assignments are written in Python 3.10.

The code is provided as is, and is not guaranteed to be correct.

PS. for other students visiting, don't just copy the code, you'll learn more by doing it yourself.