"""
INCOMPLETE!!!!

This assignment is not finished and will not be. It is only here for the off chance that I might be referenced.
"""

from typing import Tuple, Union

def parse_value(json_string: str) -> Tuple[object, str]:
    """
    Detect which type of JSON value is in the string and parse it.

    :param json_string: A JSON string starting with a value. The ending will be found by this function.
    :return: A python object and the remaining string.
    """
    if json_string[0] in {' ', '\t', '\n'}:
        return parse_value(json_string[1:])
    elif json_string[0] == '"':
        return parse_string(json_string)
    elif json_string[0] in {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-'}:
        return parse_number(json_string)
    elif json_string[0] == '[':
        return parse_array(json_string)
    elif json_string[0] == '{':
        return parse_object(json_string)
    elif json_string[0:4] == 'true':
        return True, json_string[4:]
    elif json_string[0:5] == 'false':
        return False, json_string[5:]
    elif json_string[0:4] == 'null':
        return None, json_string[4:]
    else:
        raise ValueError(f"Invalid JSON string. Couldn't detect value type: '{json_string}'")
    
def parse_string(json_string: str) -> Tuple[str, str]:
    """
    Parse a JSON string and return it as a python string, with the remaining string.

    :param json_string: A JSON string starting with '"'. The ending will be found by this function.
    :return: A string and the remaining string.
    """
    result = ''
    rest_string = json_string[1:]
    while True:
        if rest_string[0] == '"':
            return result, rest_string[1:]
        else:
            result += rest_string[0]
            rest_string = rest_string[1:]

def parse_number(json_string: str) -> Tuple[Union[int, float], str]:
    """
    Parse a JSON number and return it as a python int or float, with the remaining string.

    :param json_string: A JSON string starting with a number. The ending will be found by this function.
    :return: An int or float and the remaining string.
    """
    ending_characters = {' ', '\t', '\n', ',', ']', '}'}
    result = 0
    is_float = False
    negative = json_string[0] == '-'
    rest_string = json_string[1:] if negative else json_string
    if negative and rest_string[0] in ending_characters:
        raise ValueError(f"Invalid JSON string. Number is negative but no digits are present: '{json_string}'")
    while True:
        if rest_string[0] in ending_characters:
            return -result if negative else result, rest_string
        else:
            if rest_string[0] == '.':
                if is_float:
                    raise ValueError(f"Invalid JSON string. Number has two decimal points: '{json_string}'")
                else:
                  is_float = True
            elif rest_string[0] in {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}:
                result *= 10
                result += int(rest_string[0])
                if is_float:
                    result /= 10
            rest_string = rest_string[1:]

def parse_array(json_string: str) -> Tuple[list, str]:
    """
    Parse a JSON array and return it as a python list, with the remaining string.

    :param json_string: A JSON string starting with "[". The ending will be found by this function.
    :return: A list of objects and the remaining string.
    """
    result = []
    rest_string = json_string[1:]
    while True:
        if rest_string[0] in {' ', '\t', '\n'}:
            rest_string = rest_string[1:]
        elif rest_string[0] == ']':
            return result, rest_string[1:]
        else:
            value, rest_string = parse_value(rest_string)
            result.append(value)
            if rest_string[0] == ',':
                rest_string = rest_string[1:]

# def parse_object(json_string: str) -> Tuple[dict, str]:
#     """
#     Parse a JSON object and return it as a python dictionary, with the remaining string.
    
#     :param json_string: A JSON string starting with "{". The ending will be found by this function.
#     """
#     result = {}
#     for i, char in enumerate(json_string[1:]):
#         rest_string = json_string[i+1:]
#         if char in [' ', '\t', '\n']:
#             continue
#         elif char == '}':
#             return result, rest_string[1:]
#         elif char == '{':
#             dictionary, rest_string = parse_object(rest_string)




def parse_json(json_string):
    """Parse a JSON string and return a Python object."""
    value, rest_string = parse_value(json_string)
    if rest_string != '':
        raise ValueError(f"Invalid JSON string. Unparsed string: '{rest_string}'")
    return value



example_1 = """
{
    "fruit": "Apple",
    "size": "Large",
    "color": "Red",

    "available": true,

    "phoneNumber": null
}
"""

example_2 = """
{
    "debug": "on",
    "window": {
        "title": "Sample Konfabulator Widget",
        "name": "main_window",
        "width": 500,
        "height": 500
    },
    "image": { 
        "src": "Images/Sun.png",
        "name": "sun1",
        "hOffset": 250,
        "vOffset": 250,
        "alignment": "center"
    },
    "text": {
        "data": "Click Here",
        "size": 32.5,
        "style": "bold",
        "name": "text1",
        "hOffset": 250,
        "vOffset": 100,
        "alignment": "center",
        "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 92.5;"
    }
}
"""

example_3 = """
{"menu": {
  "id": "file",
  "value": "File",
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
}}
"""