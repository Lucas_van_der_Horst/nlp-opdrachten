from timeit import timeit
from random import sample

with open("words.txt") as file:
    words = file.read().splitlines()

N_buckets = 1000
buckets = [[] for _ in range(N_buckets)]

def hash_function(word):
    return sum(ord(c) for c in word) % N_buckets

for word in words:
    buckets[hash_function(word)].append(word)

def exaustive_search(word):
    for w in words:
        if w == word:
            return True
    return False

def hash_search(word):
    for w in buckets[hash_function(word)]:
        if w == word:
            return True
    return False

# Time the two search functions on a sample of 1000 words
sample_words = sample(words, 1000)

print("Hash search:")
print(timeit(lambda: [hash_search(word) for word in sample_words], number=1))

print("Exaustive search:")
print(timeit(lambda: [exaustive_search(word) for word in sample_words], number=1))
